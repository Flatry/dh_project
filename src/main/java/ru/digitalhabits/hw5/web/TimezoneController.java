package ru.digitalhabits.hw5.web;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.digitalhabits.hw5.dto.TimezoneDto;
import ru.digitalhabits.hw5.model.DataIn;
import ru.digitalhabits.hw5.service.DetectIpService;
import ru.digitalhabits.hw5.service.TimezoneService;
import ru.digitalhabits.hw5.service.TimezoneServiceByIp;
import javax.servlet.http.HttpServletRequest;

/**
 * 2 точки:
 *  1. Определение таймзоны по координатам - /timezone?latitssude=50&longitude=51
 *  2. Определение таймзоны по ip - /timezonebyip?ip=84.38.1.128
 *  Формат отдаваемых данных - смотри класс TimezoneDto
 */

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class TimezoneController {

    private final TimezoneService timezoneService;
    private final TimezoneServiceByIp timezoneServiceByIp;
    private final DetectIpService detectIpService;

    @GetMapping(path = "timezone", produces = MediaType.APPLICATION_JSON_VALUE)
    public TimezoneDto timezone(@RequestParam Double latitude, @RequestParam Double longitude, HttpServletRequest request) {
        return timezoneService.getTimeZone(new DataIn(latitude, longitude));
    }

    @GetMapping(path = "timezonebyip", produces = MediaType.APPLICATION_JSON_VALUE)
    public TimezoneDto timezoneByIp(@RequestParam String ip, HttpServletRequest request) {
        return timezoneServiceByIp.getTimeZone(new DataIn().setIp(ip!=null ? ip : detectIpService.getClientIpAddress(request)));
    }

}
