package ru.digitalhabits.hw5.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;

/**
 * Класс на всякий случай, для определения Ip из запроса клиента
 */

@Service
public class DetectIpService {
    @Value("${detectip.ipHeaderCandidates}")
    private String[] ipHeaderCandidates;

    public String getClientIpAddress(HttpServletRequest request) {
        String allData = "";
        for (String header : ipHeaderCandidates) {
            String ip = request.getHeader(header);
            allData += "<br />"+ip;
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }
}
