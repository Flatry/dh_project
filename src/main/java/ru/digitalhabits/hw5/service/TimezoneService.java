package ru.digitalhabits.hw5.service;

import lombok.RequiredArgsConstructor;
import ru.digitalhabits.hw5.mappers.TimezoneMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.digitalhabits.hw5.dto.TimezoneDto;
import ru.digitalhabits.hw5.model.DataIn;
import ru.digitalhabits.hw5.model.GeoNames;

/**
 * Сервис определения таймзоны по координатам
 */

@Service
@RequiredArgsConstructor
public class TimezoneService {

    private final RestTemplate restGeoNames;
    private final String geonamesUser;
    private final String geonamesUrlParams;
    private final TimezoneMapper timezoneMapper;


    public TimezoneDto getTimeZone(DataIn dataIn) {
        if (dataIn.isCoordValid()) {
            GeoNames geoNames = restGeoNames.getForObject(geonamesUrlParams, GeoNames.class, dataIn.getLatitude(), dataIn.getLatitude(), geonamesUser);
            return timezoneMapper.geoNamesToTimezoneDto(geoNames);
        }
        return null;
    }

}
