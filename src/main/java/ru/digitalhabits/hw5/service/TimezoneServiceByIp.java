package ru.digitalhabits.hw5.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.digitalhabits.hw5.dto.TimezoneDto;
import ru.digitalhabits.hw5.mappers.TimezoneMapper;
import ru.digitalhabits.hw5.model.DataIn;
import ru.digitalhabits.hw5.model.InvalideIpException;
import ru.digitalhabits.hw5.model.IpGeoLocation;

/**
 * Сервис определения таймзоны по IP
 */

@Service
@RequiredArgsConstructor
public class TimezoneServiceByIp {
    private final RestTemplate restIpGeoLocation;
    private final String ipGeoLocUrlParams;
    private final String ipGeoLocKey;
    private final TimezoneMapper timezoneMapper;

    public TimezoneDto getTimeZone(DataIn dataIn) {
        if (dataIn.isIpValid()) {
            IpGeoLocation ipGeoLocation = restIpGeoLocation.getForObject(ipGeoLocUrlParams, IpGeoLocation.class, dataIn.getIp(), ipGeoLocKey);
            return timezoneMapper.IpGeoLocationToTimezoneDto(ipGeoLocation);
        }
        throw new InvalideIpException(dataIn.getIp());
    }
}
