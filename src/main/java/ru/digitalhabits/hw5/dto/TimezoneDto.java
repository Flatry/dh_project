package ru.digitalhabits.hw5.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * DTO для возврата значений данным сервисом
 */

@Getter
@Setter
public class TimezoneDto {
    private String timezoneId;
    private String countryCode;
    private double timezoneOffset;
}
