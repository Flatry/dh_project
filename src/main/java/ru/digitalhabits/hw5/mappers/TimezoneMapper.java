package ru.digitalhabits.hw5.mappers;

import org.mapstruct.*;
import ru.digitalhabits.hw5.dto.TimezoneDto;
import ru.digitalhabits.hw5.model.GeoNames;
import ru.digitalhabits.hw5.model.IpGeoLocation;

/**
 * Маппер для конвертации "Форматы внешних сервисов -> формат, отдаваемый данным сервисом"
 */

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public abstract class TimezoneMapper {
    @Mapping(target = "timezoneId", source = "geoNames.timezoneId")
    @Mapping(target = "countryCode", source = "geoNames.countryCode")
    @Mapping(target = "timezoneOffset", source = "geoNames.gmtOffset")
    public abstract TimezoneDto geoNamesToTimezoneDto(GeoNames geoNames);

    @Mapping(target = "timezoneId", source = "ipGeoLocation.ipGeoLocationTimezone.name")
    @Mapping(target = "countryCode", source = "ipGeoLocation.countryCode")
    @Mapping(target = "timezoneOffset", source = "ipGeoLocation.ipGeoLocationTimezone.gmtOffset")
    public abstract TimezoneDto IpGeoLocationToTimezoneDto(IpGeoLocation ipGeoLocation);
}
