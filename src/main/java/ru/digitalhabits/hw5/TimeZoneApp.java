package ru.digitalhabits.hw5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "ru.digitalhabits.hw5")
public class TimeZoneApp {
	public static void main(String[] args) {
		SpringApplication.run(TimeZoneApp.class, args);
	}

}
