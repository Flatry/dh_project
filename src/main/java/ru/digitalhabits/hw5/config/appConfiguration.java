package ru.digitalhabits.hw5.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Бины с пропертей
 */

@Component
@RequiredArgsConstructor
public class appConfiguration {

    final private GeoNamesProperties geoNamesProperties;
    final private IpGeoLocProperties ipGeoLocProperties;

    @Bean(name = "restGeoNames")
    public RestTemplate getRestGeoNames(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.rootUri(geoNamesProperties.getUrlBase()).build();
    }

    @Bean(name = "geonamesUser")
    public String getGeonamesUser() {
        return geoNamesProperties.getUser();
    }

    @Bean(name = "geonamesUrlParams")
    public String getGeonamesUrlParams() {
        return geoNamesProperties.getUrlParams();
    }

    @Bean(name = "restIpGeoLocation")
    public RestTemplate getRestIpGeoLocation(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.rootUri(ipGeoLocProperties.getUrlBase()).build();
    }

    @Bean(name="ipGeoLocUrlParams")
    public String getIpGeoLocUrlParams() {
        return ipGeoLocProperties.getUrlParams();
    }

    @Bean(name = "ipGeoLocKey")
    public String getIpGeoLocKey() {
        return ipGeoLocProperties.getKey();
    }

}
