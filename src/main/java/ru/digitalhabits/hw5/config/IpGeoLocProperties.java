package ru.digitalhabits.hw5.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Конфиг для IpGeoLocation
 */

@Configuration
@ConfigurationProperties(prefix = "ipgeolocation")
@Setter
@Getter
public class IpGeoLocProperties {
    private String urlBase;
    private String urlParams;
    private String key;
}
