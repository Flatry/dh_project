package ru.digitalhabits.hw5.config;

import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;

@Configuration
@OpenAPIDefinition(info = @Info(title = "Timezone service", version = "1.0"))
public class SwaggerConfig {
}
