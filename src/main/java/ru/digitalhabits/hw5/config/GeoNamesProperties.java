package ru.digitalhabits.hw5.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Конфиг для GeoNames
 */

@Configuration
@ConfigurationProperties(prefix = "geonames")
@Setter
@Getter
public class GeoNamesProperties {
    private String user;
    private String urlBase;
    private String urlParams;
}
