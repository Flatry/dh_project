package ru.digitalhabits.hw5.model;

import lombok.Data;

@Data
public class ErrorResponse {
    private final String message;
}
