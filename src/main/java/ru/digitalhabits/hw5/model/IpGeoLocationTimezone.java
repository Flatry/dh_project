package ru.digitalhabits.hw5.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IpGeoLocationTimezone {
    private String name;
    private String abbreviation;
    private @JsonProperty("gmt_offset") String gmtOffset;
    private String current_time;
    private Boolean is_dst;
}
