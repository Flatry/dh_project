package ru.digitalhabits.hw5.model;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.validator.routines.InetAddressValidator;

/**
 * Входные параметр для удобства собираем в объект класса DataIn
 */

@Getter
@Setter
public class DataIn {
    private String ip;
    private Double latitude;
    private Double longitude;

    public DataIn(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DataIn() {}

    public DataIn setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public boolean isCoordValid() {
        return (latitude != null && longitude != null);
    }

    static public boolean isIpValid(String ip) {
        if (ip == null) {
            return false;
        }
        InetAddressValidator validator = InetAddressValidator.getInstance();
        if (validator.isValidInet4Address(ip)) {
            return true;
        }
        return validator.isValidInet6Address(ip);
    }

    public boolean isIpValid() {
        return DataIn.isIpValid(ip);
    }

}
