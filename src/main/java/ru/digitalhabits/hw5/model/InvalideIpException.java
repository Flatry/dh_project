package ru.digitalhabits.hw5.model;

/**
 * Exception Невалидный Ip
 */

public class InvalideIpException extends RuntimeException {
    public InvalideIpException(String ip) {
        super(String.format("Received ip address %s is invalid", ip));
    }
}
