package ru.digitalhabits.hw5.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Форма данных, возвращаемых сервисом GeoNames
 */

@Getter
@Setter
public class GeoNames {
    private String timezoneId;
    private String countryCode;
    public String countryName;
    private Double lng;
    private Double lat;
    private String sunrise;
    private String sunset;
    private String time;
    private Integer gmtOffset;
    private Integer rawOffset;
    private Integer dstOffset;

    @Override
    public String toString() {
        return
                timezoneId
                        + " / " + countryCode
                        + " / " + countryName
                        + " / " + lng
                        + " / " + lat
                        + " / " + sunrise
                        + " / " + sunset
                        + " / " + time
                        + " / " + gmtOffset
                        + " / " + rawOffset
                        + " / " + dstOffset
                ;
    }

}
