package ru.digitalhabits.hw5.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Форма данных, возвращаемых сервисом ipgeolocation
 */

@Getter
@Setter
public class IpGeoLocation {
    private @JsonProperty("ip_address") String ipAddress;
    private String city;
    private @JsonProperty("city_geoname_id") String cityGeonameId;
    private String region;
    private @JsonProperty("region_iso_code")  String regionIsoCode;
    private @JsonProperty("region_geoname_id") String regionGeonameId;
    private @JsonProperty("postal_code") String postalCode;
    private String country;

    private @JsonProperty("country_code") String countryCode;
    private @JsonProperty("country_geoname_id") String countryGeonameId;
    private @JsonProperty("country_is_eu") Boolean countryIsEu;
    private String continent;
    private @JsonProperty("continent_code") String continentCode;
    private @JsonProperty("continent_geoname_id") String continentGeonameId;

    private Float longitude;
    private Float latitude;
    private @JsonProperty("timezone") IpGeoLocationTimezone ipGeoLocationTimezone;
}
