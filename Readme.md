# Сервис для получения часового пояса
Сервис умеет по долготе/широте или ip-адресу получать информацию о часовом поясе: 
* код страны, 
* название часового пояса по Олсону,
* смещение в часах по Гринвичу.

## Использование
### Определение по долготе и широте
Протокол: **GET**

Точка: **/timezone**

Параметры: 
* Double **latitude**
* Double **longitude**

http://localhost:8080/timezone?latitude=50&longitude=51

### Определение по ip-адресу

Протокол: **GET**

Точка: **/timezonebyip**

Параметры:
* String **ip**

http://localhost:8080/timezonebyip?ip=84.38.1.128

### Формат выходных данных
```json
{
"timezoneId": "Europe/Moscow",
"countryCode": "RU",
"timezoneOffset": 3.0
}
```

## Сборка
```shell script
# загружает gradle wrapper
./gradlew wrapper

# сборка проекта
./gradlew clean build bootRun
```

## Docker
Сформирован image с timezone-сервисом [flatry/timezoneapp](https://hub.docker.com/repository/docker/flatry/timezoneapp/general) и загружен на [hub.docker.com](https://hub.docker.com/)

В docker-compose прописаны инструкции для закачки image и запуска timezone-сервиса.

Запуск
```shell script
docker-compose up -d
```

---
Для создания image сформированы dockerfiles
* [Dockerfile](Dockerfile) - используется multi-stage builds
* [DockerfileSimple/Dockerfile](DockerfileSimple%2FDockerfile) - простое копирование jar