FROM openjdk:14-jdk-alpine as builder
COPY gradle gradle
COPY build.gradle settings.gradle gradlew ./
COPY src src
RUN ./gradlew clean build -x test
RUN (cd build/libs; java -Djarmode=layertools -jar *.jar extract)


FROM openjdk:14-jdk-alpine
ARG DEPENDENCY=./build/libs
COPY --from=builder ${DEPENDENCY}/spring-boot-loader/ ./
COPY --from=builder ${DEPENDENCY}/dependencies/ ./
COPY --from=builder ${DEPENDENCY}/snapshot-dependencies/ ./
COPY --from=builder ${DEPENDENCY}/application/ ./
ENTRYPOINT ["java","org.springframework.boot.loader.JarLauncher"]
CMD ["noname"]